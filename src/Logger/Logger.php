<?php
namespace Logger;

use RuntimeException;
use Exception;
use DateTime;

class Logger
{
    private $fileName;
    private $maxBytes;
    private $timespan;

    public function __construct($fileName, $maxBytes = 1024 * 1024, $timespan = 5)
    {
        $this->fileName = $fileName;
        $this->maxBytes = $maxBytes;
        $this->timespan = $timespan;
    }

    public function log($message)
    {
        $message = $this->sanitize($message);
        try {
            $handler = fopen($this->fileName, 'r+');

            if (!$this->shouldThrottle($handler, $message)) {
                $this->writeFile($handler, $message);
            }
        } catch (Exception $e) {
            if (!empty($handler)) {
                fclose($handler);
            }
            throw $e;
        }

        fclose($handler);
    }

    private function shouldThrottle($handler, $message)
    {
        if (!flock($handler, LOCK_SH | LOCK_NB)) {
            return false;
        }

        fseek($handler, -$this->maxBytes, SEEK_END);
        $content = fread($handler, $this->maxBytes);
        flock($handler, LOCK_UN);

        return $this->isNotThrottled(explode("\n", $content), $message);
    }

    private function isNotThrottled($lines, $needle)
    {
        foreach ($lines as $line) {
            if (substr($line, 22) === $needle) {
                $minutesPassed = $this->getMinutesPassed(substr($line, 1, 19));

                if ($minutesPassed < $this->timespan) {
                    return true;
                }
            }
        }

        return false;
    }

    private function generateDateTime()
    {
        return '[' . DateProvider::getDate()->format('Y-m-d\TH:i:s') . ']';
    }

    private function sanitize($message)
    {
        return str_replace(["\n", "\r"], ' ', $message);
    }

    private function getMinutesPassed($dateString)
    {
        $date = new DateTime($dateString);

        $since = $date->diff(DateProvider::getDate());
        $minutes = $since->days * 24 * 60;
        $minutes += $since->h * 60;
        $minutes += $since->i;

        return $minutes;
    }

    private function writeFile($handler, $message)
    {
        if (flock($handler, LOCK_EX)) {
            fseek($handler, 0, SEEK_END);
            fwrite($handler, "{$this->generateDateTime()} {$message}\n");
            fflush($handler);
            flock($handler, LOCK_UN);
        } else {
            throw new RuntimeException('Cannot lock the log file');
        }
    }
}

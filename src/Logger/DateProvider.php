<?php
namespace Logger;

use DateTime;

class DateProvider
{
    private static $date;

    public static function getDate()
    {
        if (self::$date) {
            return self::$date;
        }

        return new DateTime();
    }

    public static function setDate(DateTime $date = null)
    {
        self::$date = $date;
    }
}

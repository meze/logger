<?php

use Logger\Logger;
use Logger\DateProvider;

class LoggerTest extends PHPUnit_Framework_TestCase
{
    private $date = '2015-07-01T10:10:10';

    public function setUp()
    {
        DateProvider::setDate(new DateTime($this->date));

        $log = fopen($this->getLogPath(), 'w');
        ftruncate($log, 0);
        fclose($log);
    }

    public function tearDown()
    {
        DateProvider::setDate(null);
    }

    public function testAppendsToLogFile()
    {
        $logger = new Logger($this->getLogPath());

        $logger->log('Hello 1');
        $logger->log('Hello 2');

        $this->assertEquals("[{$this->date}] Hello 1\n[{$this->date}] Hello 2\n", file_get_contents($this->getLogPath()));
    }

    public function testThrottlesLoggingByMessage()
    {
        $logger = new Logger($this->getLogPath(), 30);

        $logger->log('Hello 1');
        $logger->log('Hello 1');
        $logger->log('Hello 3');
        $logger->log('Hello 1');

        $this->assertEquals("[{$this->date}] Hello 1\n[{$this->date}] Hello 3\n[{$this->date}] Hello 1\n", file_get_contents($this->getLogPath()));
    }

    public function testThrottlesLoggingByDate()
    {
        $logger = new Logger($this->getLogPath(), 30, 5);

        $logger->log('Hello 1');
        DateProvider::setDate(DateProvider::getDate()->modify('+10 minutes'));
        $logger->log('Hello 1');

        $newDate = $this->date;
        $newDate[14] = 2; // earlier we added 20 mins to the current time

        $this->assertEquals("[{$this->date}] Hello 1\n[{$newDate}] Hello 1\n", file_get_contents($this->getLogPath()));
    }

    public function testTwoLoggersCanWriteLog()
    {
        $logger = new Logger($this->getLogPath());
        $logger2 = new Logger($this->getLogPath());

        $logger->log('Hello 1');
        $logger2->log('Hello 2');
        $logger->log('Hello 1');
        $logger2->log('Hello 2');

        $this->assertEquals("[{$this->date}] Hello 1\n[{$this->date}] Hello 2\n", file_get_contents($this->getLogPath()));
    }

    public function testSanitizesLogMessage()
    {
        $logger = new Logger($this->getLogPath());
        $logger->log("te\ns\rt");

        $this->assertEquals("[{$this->date}] te s t\n", file_get_contents($this->getLogPath()));
    }

    private function getLogPath()
    {
        return __DIR__ . '/fixture/log.txt';
    }
}

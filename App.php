<?php

namespace Logger;

include 'vendor/autoload.php';

class App
{
    public function run($request)
    {
        $content = file_get_contents('php://input');

        if ($content) {
            $logger = new Logger('log.txt');
            $logger->log(file_get_contents('php://input'));
        }
    }
}


if ($_SERVER['REQUEST_METHOD'] === 'OPTIONS') {
    header('Access-Control-Allow-Headers: Content-Type');
    header('Access-Control-Allow-Methods: POST');
    header('Access-Control-Allow-Origin: *');

    return;
}

$app = new App();
$app->run($_POST);
